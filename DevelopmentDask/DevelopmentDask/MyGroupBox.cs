﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DevelopmentTask
{

    class MyGroupBox : GroupBox
    {
        public enum TextAlign { Right, Center, Left };

        // プロパティ
        public Color BorderColor { get; set; } = Color.Black;
        public float PenSize { get; set; } = 2.0F;
        public TextAlign TextAlignType { get; set; } = TextAlign.Center;
        public int TextMargin { get; set; } = 0;
        public int TextPadding { get; set; } = 1;



        // コンストラクタ
        public MyGroupBox()
        {
            // ダブルバッファリングを有効
            SetStyle(ControlStyles.DoubleBuffer, true);

            // グループボックスの描画をオーナードローにする
            SetStyle(ControlStyles.UserPaint, true);
        }
        


        private float getTextX(Rectangle bordRec, SizeF textSize, TextAlign align)
        {
            switch (align)
            {
                case TextAlign.Left:
                    return TextMargin;
                case TextAlign.Center:
                    return bordRec.Width / 2.0F - textSize.Width / 2.0F;
                case TextAlign.Right:
                    return bordRec.Width - textSize.Width - TextMargin;
                default:
                    return 0;
            }
        }

        // OnPrintイベント
        protected override void OnPaint(PaintEventArgs e)
        {
            // テキストサイズを取得
            var sf = new StringFormat(StringFormat.GenericTypographic);
            var tTextSize = e.Graphics.MeasureString(this.Text, this.Font, this.Width, sf);
            //文字列Padding(線との距離)を計算
            var paddingSpace = e.Graphics.MeasureString("a", this.Font, this.Width, sf).Width * TextPadding;
            tTextSize.Width += paddingSpace * 2.0F;

            // グループボックスの領域を取得
            Rectangle tBorderRect = e.ClipRectangle;


            // テキストを考慮（グループボックス枠線がテキスト（高さ）の真ん中に来るように）して枠を描画
            tBorderRect.Y += (int)(tTextSize.Height / 2.0F);
            tBorderRect.Height -= (int)(tTextSize.Height / 2.0F);

            // Draw the rectangle with the wide green pen.
            Pen pen = new Pen(BorderColor, PenSize);
            pen.Alignment = PenAlignment.Inset;
            e.Graphics.DrawRectangle(pen, tBorderRect);
            //ControlPaint.DrawBorder3D(e.Graphics, tBorderRect, Border3DStyle.Raised, Border3DSide.All);
            //ControlPaint.DrawBorder(e.Graphics, tBorderRect, Color.White, ButtonBorderStyle.Inset);

            // テキストを描画
            RectangleF tTextRect = e.ClipRectangle;
            tTextRect.X += getTextX(tBorderRect, tTextSize, TextAlignType);    // テキストの描画開始位置(X)をalginに設定
            tTextRect.Width = tTextSize.Width;
            tTextRect.Height = tTextSize.Height;
            e.Graphics.FillRectangle(new SolidBrush(this.BackColor), tTextRect);
            e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), tTextRect.X + paddingSpace, tTextRect.Y, sf);
        }
    }
}