﻿namespace DevelopmentTask
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.productSelectionControl1 = new DevelopmentTask.ProductSelectionPage();
            this.settingPage1 = new DevelopmentTask.SettingPage();
            this.operationPage1 = new DevelopmentTask.OperationPage();
            this.historyPage1 = new DevelopmentTask.HistoryPage();
            this.manualPage1 = new DevelopmentTask.InspectionPage();
            this.SuspendLayout();
            // 
            // productSelectionControl1
            // 
            this.productSelectionControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.productSelectionControl1.BackColor = System.Drawing.Color.White;
            this.productSelectionControl1.Location = new System.Drawing.Point(12, 9);
            this.productSelectionControl1.Name = "productSelectionControl1";
            this.productSelectionControl1.Size = new System.Drawing.Size(744, 524);
            this.productSelectionControl1.TabIndex = 0;
            // 
            // settingPage1
            // 
            this.settingPage1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingPage1.Location = new System.Drawing.Point(12, 12);
            this.settingPage1.Name = "settingPage1";
            this.settingPage1.Size = new System.Drawing.Size(741, 521);
            this.settingPage1.TabIndex = 1;
            this.settingPage1.Visible = false;
            // 
            // operationPage1
            // 
            this.operationPage1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.operationPage1.Location = new System.Drawing.Point(12, 12);
            this.operationPage1.Name = "operationPage1";
            this.operationPage1.Size = new System.Drawing.Size(741, 521);
            this.operationPage1.TabIndex = 2;
            this.operationPage1.Visible = false;
            // 
            // historyPage1
            // 
            this.historyPage1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyPage1.Location = new System.Drawing.Point(12, 12);
            this.historyPage1.Name = "historyPage1";
            this.historyPage1.Size = new System.Drawing.Size(741, 521);
            this.historyPage1.TabIndex = 3;
            this.historyPage1.Visible = false;
            // 
            // manualPage1
            // 
            this.manualPage1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.manualPage1.Location = new System.Drawing.Point(12, 12);
            this.manualPage1.Name = "manualPage1";
            this.manualPage1.Size = new System.Drawing.Size(741, 521);
            this.manualPage1.TabIndex = 4;
            this.manualPage1.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 545);
            this.Controls.Add(this.settingPage1);
            this.Controls.Add(this.productSelectionControl1);
            this.Controls.Add(this.operationPage1);
            this.Controls.Add(this.manualPage1);
            this.Controls.Add(this.historyPage1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "ゴム製品袋詰め装置";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ProductSelectionPage productSelectionControl1;
        private SettingPage settingPage1;
        private OperationPage operationPage1;
        private HistoryPage historyPage1;
        private InspectionPage manualPage1;
    }
}

