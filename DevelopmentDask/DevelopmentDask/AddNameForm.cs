﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace DevelopmentTask
{
    public partial class AddNameForm : Form
    {
        private string addName;
        private Process process;

        public AddNameForm()
        {
            InitializeComponent();
            this.textBox1.VisibleChanged += (sender, e) => {
                if (this.Visible)
                {
                    process = new Process();
                    process.StartInfo.FileName = "osk.exe";
                    process.Start();
                }
            };
            this.Disposed += (sender, e) =>
            {
                if(!process.HasExited)
                    process.Kill();
            };

            this.StartPosition = FormStartPosition.CenterScreen;
            addName = null;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;

            if (this.textBox1.Text.Length != 0)
                addName = this.textBox1.Text;
            else
                addName = null;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        public string AddName
        {
            get { return addName; }
        }
    }
}
