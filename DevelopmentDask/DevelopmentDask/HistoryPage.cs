﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;
using System.Reflection;

namespace DevelopmentTask
{
    public partial class HistoryPage : UserControl
    {
        public ProductSelectionPage BackPage;
        private List<string> workHistoryString;
        private List<string> allWorkerName;
        private Dictionary<string, Series> seriesList = new Dictionary<string, Series>();  //<作業者名,　Series>
        private Dictionary<string, int> dateToIndex = new Dictionary<string, int>();
        private Dictionary<string, Chart> graph = new Dictionary<string, Chart>();
        private DataTable dataTable = new DataTable("table");
        private List<string> checkedNameList = new List<string>();
        private List<string> unCheckedNameList = new List<string>();

        private int height = 400;
        private string oldSelectDate = "";

        public HistoryPage()
        {
            InitializeComponent();
            this.Visible = false;
            this.VisibleChanged += this.visibleChanged;

            dataTable.Columns.Add("日付", Type.GetType("System.DateTime"));
            dataTable.Columns.Add("商品種類", Type.GetType("System.String"));
            dataTable.Columns.Add("作業者名", Type.GetType("System.String"));
            dataTable.Columns.Add("作業時間", Type.GetType("System.TimeSpan"));
            dataTable.Columns.Add("検査ワーク数", Type.GetType("System.Int32"));
            dataTable.Columns.Add("製造袋数", Type.GetType("System.Int32"));

            this.initComboBoxs();
            this.unitComboBox.SelectedIndexChanged += unitComboBox_SelectedIndexChanged;
            this.kindComboBox.SelectedIndexChanged += kindComboBox_SelectedIndexChanged;
            this.kind2ComboBox.SelectedIndexChanged += kindComboBox_SelectedIndexChanged;

            this.yearComboBox.SelectionChangeCommitted += new EventHandler(this.month_DateSelected);
            this.monthComboBox.SelectionChangeCommitted += new EventHandler(this.month_DateSelected);
            this.nameCheckedListBox.SelectedIndexChanged += new EventHandler(checkedListBox1_SelectedIndexChanged);
        }

        private void initComboBoxs()
        {
            int nowYear = DateTime.Now.Year;
            foreach (var i in Enumerable.Range(2018, (nowYear - 2018) + 1))
                this.yearComboBox.Items.Add(i.ToString());

            foreach (var i in Enumerable.Range(1, 12))
                this.monthComboBox.Items.Add(i.ToString());

            this.yearComboBox.SelectedItem = nowYear.ToString();
            this.monthComboBox.SelectedItem = DateTime.Now.Month.ToString();
            this.unitComboBox.SelectedIndex = 0;
            this.kindComboBox.SelectedIndex = 0;
            this.kind2ComboBox.SelectedIndex = 0;
        }

        private void HistoryPage_Load(object sender, EventArgs e)
        {
            var w = this.myGroupBox1.Width * this.tableLayoutPanel4.ColumnStyles[0].Width / 100.0F;

            this.yearComboBox.Size = new Size((int)w, this.yearComboBox.Height);
            this.monthComboBox.Size = new Size((int)w, this.yearComboBox.Height);
        }

        private void month_DateSelected(object sender, EventArgs e)
        {
            var year = this.yearComboBox.SelectedItem.ToString();
            var month = this.monthComboBox.SelectedItem.ToString();

            //表示する年月が変更された場合、グラフとデータテーブルを初期化
            if (oldSelectDate != (year + month))
            {
                graph.Clear();
                seriesList.Clear();
                dataTable.Clear();
                this.panel1.Controls.Clear();

                loadHistoryData(year);
            }

            List<DataRow> dataRows = new List<DataRow>();
            var list = dataTable.Select().ToList();
            if (this.nameCheckedListBox.GetItemChecked(0))
                dataRows = selectDataRow(list, allWorkerName);
            else
                dataRows = selectDataRow(list, checkedNameList);


            if (dataRows.Count() > 0)
            {
                this.dataGridView1.DataSource = dataRows.CopyToDataTable();
                drawDiagram(dataRows, checkedNameList, unCheckedNameList);
            }
            else
            {
                var data = dataTable.Copy();
                data.Rows.Clear();
                this.dataGridView1.DataSource = data;
                drawDiagram(null, null, allWorkerName);
            }

            oldSelectDate = year + month;
        }

        private void unitComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (unitComboBox.SelectedIndex == 0)
                monthComboBox.Enabled = true;
            else
                monthComboBox.Enabled = false;

            this.panel1.Controls.Clear();
            graph.Clear();
            seriesList.Clear();
            month_DateSelected(sender, e);

        }

        private void kindComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.panel1.Controls.Clear();
            graph.Clear();
            seriesList.Clear();
            month_DateSelected(sender, e);
            
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var result = this.nameCheckedListBox.GetItemChecked(0);

            if(this.nameCheckedListBox.CheckedItems.Count  <= 0)
            {
                //作業者が選択されていないとき、グラフとデータグリッドを非表示
                //グラフを非表示
                drawDiagram(null, null, allWorkerName);

                //データグリッドを初期化
                var data = dataTable.Copy();
                data.Rows.Clear();
                this.dataGridView1.DataSource = data;
                checkedNameList.Clear();
                unCheckedNameList = new List<string>(allWorkerName);

                return;
            }
            else if (result && this.nameCheckedListBox.SelectedItem.Equals("ALL"))//ALLが選択されるとほかのチェックを外す
            {
                for (var i = 1; i < this.nameCheckedListBox.Items.Count; i++)
                    this.nameCheckedListBox.SetItemChecked(i, false);

                checkedNameList = new List<string>(allWorkerName);
                unCheckedNameList.Clear();
            }
            else if (result && (this.nameCheckedListBox.CheckedItems.Count > 1))//ALL以外をチェックするとALLのチェックを外す
            {
                checkedNameList.Clear();
                this.nameCheckedListBox.SetItemChecked(0, false);
                var name = this.nameCheckedListBox.SelectedItem.ToString();
                checkedNameList.Add(name);
                unCheckedNameList = new List<string>(allWorkerName);
                unCheckedNameList.Remove(name);
            }
            else
            {
                var name = this.nameCheckedListBox.SelectedItem.ToString();
                if (this.nameCheckedListBox.GetItemChecked(this.nameCheckedListBox.SelectedIndex))
                {
                    checkedNameList.Add(name);
                    unCheckedNameList.Remove(name);
                }
                else
                {
                    checkedNameList.Remove(name);
                    unCheckedNameList.Add(name);
                }

            }

            month_DateSelected(sender, e);


        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.BackPage.Visible = true;
            this.Visible = false;
        }

        private List<string> readWorkHistoryFile(string year)
        {

            string path = String.Format(MainForm.RootPath + @"\data\history\{0}.csv", year);

            try
            {
                using (StreamReader workerName = new StreamReader(path))
                {
                    string line;
                    List<string> list = new List<string>();
                    int count = 0;

                    while ((line = workerName.ReadLine()) != null)
                    {
                        list.Add(line);
                        count++;
                    }

                    if (count > 0)
                        return list;
                    else
                        return null;

                }
            }
            catch (FileNotFoundException e)
            {
                MessageBoxSet.Show("選択した月の作業履歴ファイルが存在しません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            catch (DirectoryNotFoundException e)
            {
                MessageBoxSet.Show("選択した月のフォルダーが存在しません", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void visibleChanged(object sender, EventArgs e)
        {
            if (this.Visible == true)
            {
                try
                {
                    var year = this.yearComboBox.SelectedItem.ToString();
                    allWorkerName = new List<string>(BackPage.WorkerList);
                    checkedNameList = new List<string>(BackPage.WorkerList);
                    unCheckedNameList.Clear();
                    this.nameCheckedListBox.Items.Clear();
                    this.nameCheckedListBox.Items.Add("ALL");
                    this.nameCheckedListBox.Items.AddRange(allWorkerName.ToArray());

                    foreach (var index in Enumerable.Range(1, this.nameCheckedListBox.Items.Count - 1))
                    {
                        var name = this.nameCheckedListBox.Items[index].ToString();
                        if (checkedNameList.Contains(name))
                            this.nameCheckedListBox.SetItemCheckState(index, CheckState.Checked);
                    }

                    this.panel1.Controls.Clear();//追加1212
                    graph.Clear();//追加1212
                    dataTable.Clear();//追加1212
                    loadHistoryData(year);
                    month_DateSelected(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBoxSet.Show("エラー", "ERR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MessageBoxSet.Show(ex.ToString());
                }
            }
        }

        private void drawDiagram(List<DataRow> historyData, List<string> checkedWorkerName, List<string> unCheckedWorkerName)
        {
            unVisibleDiagram(unCheckedWorkerName);
            setDiagram(historyData, checkedWorkerName);
        }

        private void unVisibleDiagram(List<string> unCheckedWorkerName)
        {
            if(unCheckedWorkerName != null)
            {
                foreach (var name in unCheckedWorkerName)
                {
                    if (graph.ContainsKey(name))
                        graph[name].Visible = false;
                }
            }
        }

        private void setDiagram(List<DataRow> datas, List<string> checkedWorkerName)
        {
            if (checkedWorkerName == null)
                return;

            var point = new Point(-this.panel1.HorizontalScroll.Value, -this.panel1.VerticalScroll.Value);
            foreach (var name in checkedWorkerName)
            {
                if (graph.ContainsKey(name))
                {
                    graph[name].Visible = true;
                    graph[name].Location = point;
                    point = new Point(0, point.Y + height);
                    continue;
                }

                var rowdata = datas.Where(x => x.Field<String>("作業者名").Equals(name)).ToList();
                if (rowdata.Count <= 0)
                    continue;

                var chart = new Chart();
                chart.ChartAreas.Add(new ChartArea(name));
                chart.Series.Add(name);
                chart.Series[name] = createSeries(rowdata);
                chart.Series[name].Color = Color.FromKnownColor((KnownColor)graph.Count + 1);
                chart.ChartAreas[name].AxisY.MajorGrid.Enabled = false;
                chart.ChartAreas[name].AxisX.Interval = 1;
                chart.ChartAreas[name].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dot;
                chart.Size = new Size(this.panel1.Width, height);
                chart.Location = point;

                var leg = new Legend();
                leg.DockedToChartArea = name;
                leg.Alignment = StringAlignment.Near;
                chart.Legends.Add(leg);

                graph.Add(name, chart);
                this.panel1.Controls.Add(graph[name]);
                point = new Point(0, point.Y + height);
            }
        }

        private Series createSeries(List<DataRow> data)
        {
            Series series;
            Dictionary<int, int> pairs = new Dictionary<int, int>();

            int xDataNum = 0;
            string fieldName;
            Func<DataRow, int> func;
            if (this.kind2ComboBox.SelectedIndex == 0)
                fieldName = "検査ワーク数";
            else
                fieldName = "製造袋数";

            if (this.unitComboBox.SelectedIndex == 0)
            {
                func = d =>
                {
                    return d.Field<DateTime>("日付").Day;
                };
                var year = Convert.ToInt32(this.yearComboBox.SelectedItem.ToString());
                var month = Convert.ToInt32(this.monthComboBox.SelectedItem.ToString());

                xDataNum = DateTime.DaysInMonth(year, month);
            }
            else
            {
                func = d =>
                {
                    return d.Field<DateTime>("日付").Month;
                };

                xDataNum = 12;
            }


            pairs = calcProductNum(data, func, fieldName);
            series = createSeriesPoint(xDataNum, pairs);
            series.ChartType = SeriesChartType.Column;
            series.LegendText = data[0].Field<string>("作業者名");
            series.BorderWidth = 2;
            series.MarkerSize = 1;
            series.Name = data[0].Field<string>("作業者名");
            series.ChartArea = data[0].Field<string>("作業者名");

            return series;
        }

        private Dictionary<int, int> calcProductNum(List<DataRow> datas, Func<DataRow, int> func, string fieldNameWorkType = "検査ワーク数")
        {
            Dictionary<int, int> keyValues = new Dictionary<int, int>();
            foreach (var data in datas)
            {
                var date = func(data);
                /*if (data.Field<Int32>(fieldNameWorkType) == 0)
                    continue;*/
                if (keyValues.ContainsKey(date))
                    keyValues[date] += data.Field<Int32>(fieldNameWorkType);
                else
                    keyValues.Add(date, data.Field<Int32>(fieldNameWorkType));
            }
            return keyValues;
        }

        private Series createSeriesPoint(int xNum, Dictionary<int, int> keyValue)
        {
            var series = new Series();

            for (int index = 0; index < xNum; index++)
            {
                var date = index + 1;
                if (keyValue.ContainsKey(date))
                {
                    series.Points.Add(new DataPoint(index, keyValue[date]));
                    series.Points[index].Label = keyValue[date].ToString();
                }
                else
                {
                    series.Points.Add(new DataPoint(index, Double.NaN));
                }
                series.Points[index].AxisLabel = date.ToString();
            }

            return series;
        }

        private List<DataRow> selectDataRow(List<DataRow> datas, List<string> nameList)
        {
            var c1 = this.unitComboBox.SelectedIndex;
            var c2 = this.kindComboBox.SelectedIndex;
            var year = Convert.ToInt32(this.yearComboBox.SelectedItem.ToString());
            var month = Convert.ToInt32(this.monthComboBox.SelectedItem.ToString());

            var d = datas.Where(x => nameList.Any(key => x.Field<string>("作業者名").Equals(key)));

            switch (c1)
            {
                case 0:
                    d = d.Where(x => x.Field<DateTime>("日付").Month == month);
                    break;

                case 1:
                    break;
            }
            switch (c2)
            {
                case 0:
                    break;

                case 1:
                    d = d.Where(x => x.Field<String>("商品種類") == "大部品");
                    break;

                case 2:
                    d = d.Where(x => x.Field<String>("商品種類") == "小部品");
                    break;
            }
            return d.ToList();
        }

        private void loadHistoryData(string year)
        {
            //選択された年月のcsvファイルを読み込み
            workHistoryString = readWorkHistoryFile(year);
            if (workHistoryString == null)
                return;

            //文字列データをRowDataに変換
            foreach (var line in workHistoryString)
            {
                var data = line.Split(',');
                var dateSplit = data[0].Split('/');
                var date = new DateTime(Convert.ToInt32(year), Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]));
                var hms = Array.ConvertAll(data[3].Split(':'), int.Parse);
                var time = new TimeSpan(hms[0], hms[1], hms[2]);
                dataTable.Rows.Add(date, data[1], data[2], time, data[4], data[5]);
            }
        }
    }

}
