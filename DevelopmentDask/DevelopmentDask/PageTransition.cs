﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace DevelopmentDask
{
    public class PageTransition
    {
        public dynamic PageTransitionPoint = new ExpandoObject();

        public PageTransition()
        {

        }

        public bool transition(string pageName)
        {
            IDictionary<string, object> page = PageTransitionPoint;

            if (pageName == null)
            {
                return false;
            }
            if (page.ContainsKey(pageName) == false)
            {
                return false;
            }
            else
            {
                dynamic obj = page[pageName];
                obj.Visible = true;
                return true;
            }
        }
    }
}
