﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;

namespace DevelopmentTask
{
    public partial class MainForm : Form
    {
        static public Action<string> seriarReadEvent;
        static public string RootPath = (Environment.Is64BitProcess) ? @"../../../" : @"../../";
        static public int ClampCount { get; set; } = 0;
        private const int MAX_CLAMP_COUNT = 300;

        public MainForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.FormClosing += this.exitCancel;

            this.productSelectionControl1.Exit_Click += (sender, e) =>
                                                                    {
                                                                        this.FormClosing -= this.exitCancel;
                                                                        SerialCommunication.closeSerialPort();
                                                                        writeClampCountFile(ClampCount.ToString());
                                                                        Application.Exit();
                                                                        };

            productSelectionControl1.NextPage = settingPage1;
            productSelectionControl1.WorkHistoryPage = historyPage1;
            productSelectionControl1.InspectionPage_ = manualPage1;
            historyPage1.BackPage = productSelectionControl1;
            settingPage1.PreviousPage = productSelectionControl1;
            settingPage1.NextPage = operationPage1;
            operationPage1.productSelection = productSelectionControl1;
            operationPage1.manualPage = manualPage1;

            MessageBoxSet.NonMsgNet_Init();//追加0110

            Error.loadJson(this);
            var count = readClampCountFile();
            if(count != null)
                ClampCount = Convert.ToInt32(count);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SerialCommunication.readEventHandle += invorkFunction;

            if (ClampCount >= MAX_CLAMP_COUNT)
            {
                var text = String.Format("圧着工程回数が{0}回に達しています\nテフロンシートを交換してください", ClampCount);
                MessageBoxSet.Show(text, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                var result = MessageBoxSet.Show("圧着工程回数をリセットしますか?", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                ClampCount = (result == DialogResult.Yes) ? 0 : ClampCount;
            }
                
        }

        static private string readClampCountFile()
        {
            using (var workerName = new StreamReader(MainForm.RootPath + @"\data\clampCount.data"))
            {
                string line;

                if ((line = workerName.ReadLine()) != null)
                    return line;
                else
                    return null;

            }
        }

        static private void writeClampCountFile(string count)
        {
            using (var workerName = new StreamWriter(MainForm.RootPath + @"\data\clampCount.data", false))
            {
                workerName.WriteLine(count);
            }
        }

        public void emergencyReset(string dummy)
        {
            historyPage1.Visible = false;
            settingPage1.Visible = false;
            operationPage1.Visible = false;
            manualPage1.Visible = false;
            productSelectionControl1.Visible = true;

        }

        private void exitCancel(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        public void invorkFunction(string str)
        {
            if (seriarReadEvent != null)
                Invoke(seriarReadEvent, str);
        }

    }
}
