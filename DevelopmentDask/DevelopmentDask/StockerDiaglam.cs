﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopmentTask
{
    public partial class StockerDiaglam : UserControl
    {
        public string rightStockerTxt
        {
            get { return label2.Text; }
            set { label2.Text = value; }
        }

        public string leftStockerTxt
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public string secondStockerTxt
        {
            get { return label3.Text; }
            set { label3.Text = value; }
        }

        public float ImageScale { get; set; } = 1.0F;

        public StockerDiaglam()
        {
            InitializeComponent();
        }

        private void StockerDiaglam_Load(object sender, EventArgs e)
        {
            var img = MyUtility.creatGraphicImage(Properties.Resources.stocker, this.Width, this.Height);
            pictureBox1.Image = MyUtility.creatGraphicImage(new Bitmap(img), ImageScale);
            var point = MyUtility.calcCenterPoint(this, pictureBox1.Image);
            pictureBox1.Width = pictureBox1.Image.Width;
            pictureBox1.Height = pictureBox1.Image.Height;
            pictureBox1.Location = Point.Round(point);

            Size tTextSize = TextRenderer.MeasureText(label1.Text, label1.Font);

            var w = pictureBox1.Image.Width * 0.25;
            label1.Width = (int)w;
            label2.Width = (int)w;
            label3.Width = (int)w;

            label1.Height = tTextSize.Height;
            label2.Height = tTextSize.Height;
            label3.Height = tTextSize.Height;

            var x = pictureBox1.Image.Width * 0.23 - w / 2 + pictureBox1.Location.X;
            var y = pictureBox1.Image.Height * 0.33 - label1.Height / 2 + pictureBox1.Location.Y;
            label1.Location = new Point((int)x, (int)y);

            x = pictureBox1.Image.Width * 0.76 - w / 2 + pictureBox1.Location.X;
            label2.Location = new Point((int)x, (int)y);

            x = pictureBox1.Image.Width * 0.5 - w / 2 + pictureBox1.Location.X;
            y = pictureBox1.Image.Height * 0.75 - label3.Height / 2 + pictureBox1.Location.Y;
            label3.Location = new Point((int)x, (int)y);
        }
    }
}
