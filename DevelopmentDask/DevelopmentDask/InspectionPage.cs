﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace DevelopmentTask
{
    public partial class InspectionPage : UserControl
    {
        private int stockerR = 0;
        private int stockerL = 0;
        OperationPage operationPage;
        private bool clampFlag = false;

        const bool ON   = true;
        const bool OFF  = false;

        public InspectionPage()
        {
            InitializeComponent();
            this.Visible = false;
            this.VisibleChanged += this.visibleChanged;
        }

        private void InspectionPage_Load(object sender, EventArgs e)
        {
            var img = MyUtility.creatGraphicImage(Properties.Resources.machine, this.panel1.Width, this.panel1.Height);
            this.pictureBox1.Image = img;
            this.pictureBox1.Size = img.Size;
            this.pictureBox1.Location = Point.Round(MyUtility.calcCenterPoint(this.panel1, img));
        }

        public dynamic BackPage { get; set; }

        private void workNumPlusButton_Click(object sender, EventArgs e)
        {
            if (stockerL + stockerR < operationPage.WorkTargetNum - operationPage.SecondStockerNum)
            {
                var btn = (Button)sender;
                if (btn.Name.Split('_')[1].Equals("R"))
                {
                    stockerR++;
                    operationPage.RightStockerDiff++;
                }
                else
                {
                    stockerL++;
                    operationPage.LeftStockerDiff++;
                }

                updateLabelText();
            }
        }
        private void workNumMinusButton_Click(object sender, EventArgs e)
        {
            var btn = (Button)sender;
            if (btn.Name.Split('_')[1].Equals("R"))
            {
                if(stockerR > 0)
                {
                    stockerR--;
                    operationPage.RightStockerDiff--;
                }
            }
            else
            {
                if (stockerL > 0)
                {
                    stockerL--;
                    operationPage.LeftStockerDiff--;
                }
            }  

            updateLabelText();
        }

        private void clampButton_Click(object sender, EventArgs e)
        {
            ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, ON, false);
            clampFlag = true;
        }

        private void checkBoxStateChanged(object sender, EventArgs @event)
        {
            CheckBox checkBox = (CheckBox)sender;
            ControlOrder.OrderName order = (ControlOrder.OrderName)Enum.ToObject(typeof(ControlOrder.OrderName), checkBox.TabIndex + 1);
            
            var str = checkBox.Text.Split('_');

            if (checkBox4.Checked && checkBox == checkBox5)
                checkBox5.Checked = false;

            if (checkBox.CheckState == CheckState.Checked)
            {
                checkBox.Text = str[0] + "_OFF";
                ControlOrder.orderExecute(order, ON, true);
            }
            else
            {
                checkBox.Text = str[0] + "_ON";
                ControlOrder.orderExecute(order, OFF, true);
            }

            resetNum_Click(checkBox);

        }

        private void resetNum_Click(CheckBox checkBox)
        {
            if (operationPage == null)
                return;

            if (checkBox.TabIndex == 0)
            {
                operationPage.SecondStockerNum += stockerL;
                operationPage.LeftStockerDiff = 0;
                stockerL = 0;
                updateLabelText();
            }
            else if (checkBox.TabIndex == 1)
            {
                operationPage.SecondStockerNum += stockerR;
                operationPage.RightStockerDiff = 0;
                stockerR = 0;
                updateLabelText();
            }
        }

        private void backPageButton_Click(object sender, EventArgs e)
        {
            if (!clampFlag)
            {
                this.Visible = false;
                BackPage.Visible = true;
                BackPage = null;
            }
        }

        private void updateLabelText()
        {
            this.rightStockerNum.Text = stockerR.ToString() + "/" + operationPage.WorkTargetNum;
            this.leftStockerNum.Text = stockerL.ToString() +  "/" + operationPage.WorkTargetNum;
        }

        private void cycleExit(string dummy)
        {
            ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, OFF);
            clampFlag = false;
        }

        private void visibleChanged(object sender, EventArgs e)
        {

            if (this.Visible)
            {
                if (BackPage.GetType() == typeof(OperationPage))
                {
                    operationPage = (OperationPage)BackPage;
                    stockerR = operationPage.RightStockerNum;
                    stockerL = operationPage.LeftStockerNum;
                    updateLabelText();
                    this.myGroupBox2.Visible = true;
                }
                else
                {
                    this.myGroupBox2.Visible = false;  
                }

                SerialCommunication.addEventList("00A1", cycleExit);
            }
            else
            {
                bool step;
                if(BackPage.GetType() == typeof(OperationPage))
                {
                    operationPage.RightStockerNum = stockerR;
                    operationPage.LeftStockerNum = stockerL;
                    step = false;
                }
                else
                {
                    ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, OFF);//M0000信号OFF0111
                    step = true;
                }

                SerialCommunication.dellEventList("00A1", cycleExit);

                if (clampFlag)
                {
                    ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, OFF);
                }
                clampFlag = false;

                foreach (var cb in this.tableLayoutPanel2.Controls)
                {
                    var checkBox = (CheckBox)cb;
                    ControlOrder.OrderName order = (ControlOrder.OrderName)Enum.ToObject(typeof(ControlOrder.OrderName), checkBox.TabIndex + 1);

                    var str = checkBox.Text.Split('_');
                    
                    if (checkBox.CheckState == CheckState.Checked)
                    {
                        checkBox.Checked = false;
                        checkBox.Text = str[0] + "_ON";
                        ControlOrder.orderExecute(order, OFF, step);
                    }
                }
            }
                
        }

    }
}
