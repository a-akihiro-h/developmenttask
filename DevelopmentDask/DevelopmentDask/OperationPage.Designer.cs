﻿namespace DevelopmentTask
{
    partial class OperationPage
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.EndWorkButton = new System.Windows.Forms.Button();
            this.manualButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.myGroupBox1 = new DevelopmentTask.MyGroupBox();
            this.stockerDiaglam1 = new DevelopmentTask.StockerDiaglam();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.myGroupBox2 = new DevelopmentTask.MyGroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.finishedProductNumLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.myGroupBox3 = new DevelopmentTask.MyGroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.nowTimeLabel = new System.Windows.Forms.Label();
            this.myGroupBox4 = new DevelopmentTask.MyGroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.passingTimeLabel = new System.Windows.Forms.Label();
            this.Load += OperationPage_Load;
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.myGroupBox1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.myGroupBox2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.myGroupBox3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.myGroupBox4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.EndWorkButton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.manualButton, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.myGroupBox3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.myGroupBox4, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(767, 690);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // EndWorkButton
            // 
            this.EndWorkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EndWorkButton.Font = new System.Drawing.Font("MS UI Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.EndWorkButton.Location = new System.Drawing.Point(622, 10);
            this.EndWorkButton.Margin = new System.Windows.Forms.Padding(10);
            this.EndWorkButton.Name = "EndWorkButton";
            this.tableLayoutPanel1.SetRowSpan(this.EndWorkButton, 3);
            this.EndWorkButton.Size = new System.Drawing.Size(135, 463);
            this.EndWorkButton.TabIndex = 1;
            this.EndWorkButton.Text = "作業終了";
            this.EndWorkButton.UseVisualStyleBackColor = true;
            this.EndWorkButton.Click += new System.EventHandler(this.EndWorkButton_Click);
            // 
            // manualButton
            // 
            this.manualButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.manualButton.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.manualButton.Location = new System.Drawing.Point(632, 503);
            this.manualButton.Margin = new System.Windows.Forms.Padding(20);
            this.manualButton.Name = "manualButton";
            this.manualButton.Size = new System.Drawing.Size(115, 167);
            this.manualButton.TabIndex = 2;
            this.manualButton.Text = "手動操作";
            this.manualButton.UseVisualStyleBackColor = true;
            this.manualButton.Click += new System.EventHandler(this.manualButton_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.myGroupBox1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(38, 138);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(268, 552);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // myGroupBox1
            // 
            this.myGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.myGroupBox1.BorderColor = System.Drawing.Color.Black;
            this.myGroupBox1.Controls.Add(this.stockerDiaglam1);
            this.myGroupBox1.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.myGroupBox1.Location = new System.Drawing.Point(3, 58);
            this.myGroupBox1.Name = "myGroupBox1";
            this.myGroupBox1.PenSize = 2F;
            this.myGroupBox1.Size = new System.Drawing.Size(262, 435);
            this.myGroupBox1.TabIndex = 3;
            this.myGroupBox1.TabStop = false;
            this.myGroupBox1.Text = "ストッカ状況";
            this.myGroupBox1.TextAlignType = DevelopmentTask.MyGroupBox.TextAlign.Center;
            this.myGroupBox1.TextMargin = 0;
            this.myGroupBox1.TextPadding = 1;
            // 
            // stockerDiaglam1
            // 
            this.stockerDiaglam1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stockerDiaglam1.ImageScale = 1F;
            this.stockerDiaglam1.leftStockerTxt = "label1";
            this.stockerDiaglam1.Location = new System.Drawing.Point(3, 38);
            this.stockerDiaglam1.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.stockerDiaglam1.Name = "stockerDiaglam1";
            this.stockerDiaglam1.rightStockerTxt = "label2";
            this.stockerDiaglam1.secondStockerTxt = "label3";
            this.stockerDiaglam1.Size = new System.Drawing.Size(256, 394);
            this.stockerDiaglam1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.myGroupBox2, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(344, 138);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(268, 552);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // myGroupBox2
            // 
            this.myGroupBox2.BorderColor = System.Drawing.Color.Black;
            this.myGroupBox2.Controls.Add(this.tableLayoutPanel4);
            this.myGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myGroupBox2.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.myGroupBox2.Location = new System.Drawing.Point(3, 58);
            this.myGroupBox2.Name = "myGroupBox2";
            this.myGroupBox2.PenSize = 2F;
            this.myGroupBox2.Size = new System.Drawing.Size(262, 435);
            this.myGroupBox2.TabIndex = 0;
            this.myGroupBox2.TabStop = false;
            this.myGroupBox2.Text = "袋詰め完了数";
            this.myGroupBox2.TextAlignType = DevelopmentTask.MyGroupBox.TextAlign.Center;
            this.myGroupBox2.TextMargin = 0;
            this.myGroupBox2.TextPadding = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.finishedProductNumLabel, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(256, 394);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // finishedProductNumLabel
            // 
            this.finishedProductNumLabel.AutoSize = true;
            this.finishedProductNumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.finishedProductNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.finishedProductNumLabel.Location = new System.Drawing.Point(3, 78);
            this.finishedProductNumLabel.Name = "finishedProductNumLabel";
            this.finishedProductNumLabel.Size = new System.Drawing.Size(122, 236);
            this.finishedProductNumLabel.TabIndex = 0;
            this.finishedProductNumLabel.Text = "0";
            this.finishedProductNumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(131, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 236);
            this.label2.TabIndex = 1;
            this.label2.Text = "袋";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // myGroupBox3
            // 
            this.myGroupBox3.BorderColor = System.Drawing.Color.Black;
            this.myGroupBox3.Controls.Add(this.tableLayoutPanel5);
            this.myGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myGroupBox3.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.myGroupBox3.Location = new System.Drawing.Point(41, 3);
            this.myGroupBox3.Name = "myGroupBox3";
            this.myGroupBox3.PenSize = 2F;
            this.myGroupBox3.Size = new System.Drawing.Size(262, 132);
            this.myGroupBox3.TabIndex = 6;
            this.myGroupBox3.TabStop = false;
            this.myGroupBox3.Text = "現在時刻";
            this.myGroupBox3.TextAlignType = DevelopmentTask.MyGroupBox.TextAlign.Center;
            this.myGroupBox3.TextMargin = 0;
            this.myGroupBox3.TextPadding = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.nowTimeLabel, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(256, 91);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // nowTimeLabel
            // 
            this.nowTimeLabel.AutoSize = true;
            this.nowTimeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nowTimeLabel.Font = new System.Drawing.Font("MS UI Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.nowTimeLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.nowTimeLabel.Location = new System.Drawing.Point(3, 9);
            this.nowTimeLabel.Name = "nowTimeLabel";
            this.nowTimeLabel.Size = new System.Drawing.Size(250, 72);
            this.nowTimeLabel.TabIndex = 0;
            this.nowTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // myGroupBox4
            // 
            this.myGroupBox4.BorderColor = System.Drawing.Color.Black;
            this.myGroupBox4.Controls.Add(this.tableLayoutPanel6);
            this.myGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myGroupBox4.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.myGroupBox4.Location = new System.Drawing.Point(347, 3);
            this.myGroupBox4.Name = "myGroupBox4";
            this.myGroupBox4.PenSize = 2F;
            this.myGroupBox4.Size = new System.Drawing.Size(262, 132);
            this.myGroupBox4.TabIndex = 7;
            this.myGroupBox4.TabStop = false;
            this.myGroupBox4.Text = "経過時刻";
            this.myGroupBox4.TextAlignType = DevelopmentTask.MyGroupBox.TextAlign.Center;
            this.myGroupBox4.TextMargin = 0;
            this.myGroupBox4.TextPadding = 1;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.passingTimeLabel, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(256, 91);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // passingTimeLabel
            // 
            this.passingTimeLabel.AutoSize = true;
            this.passingTimeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passingTimeLabel.Font = new System.Drawing.Font("MS UI Gothic", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.passingTimeLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.passingTimeLabel.Location = new System.Drawing.Point(3, 9);
            this.passingTimeLabel.Name = "passingTimeLabel";
            this.passingTimeLabel.Size = new System.Drawing.Size(250, 72);
            this.passingTimeLabel.TabIndex = 0;
            this.passingTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OperationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "OperationPage";
            this.Size = new System.Drawing.Size(767, 690);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.myGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.myGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.myGroupBox3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.myGroupBox4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button EndWorkButton;
        private System.Windows.Forms.Button manualButton;
        private MyGroupBox myGroupBox1;
        private StockerDiaglam stockerDiaglam1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MyGroupBox myGroupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label finishedProductNumLabel;
        private System.Windows.Forms.Label label2;
        private MyGroupBox myGroupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label nowTimeLabel;
        private MyGroupBox myGroupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label passingTimeLabel;
    }
}
