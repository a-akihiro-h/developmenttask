﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace DevelopmentTask
{
    static class Error
    {
        static private List<Errors> errors = new List<Errors>();
        static private Dictionary<string, ErrorForm> errorForm = new Dictionary<string, ErrorForm>();
        static private MainForm mainForm;
        static private int formInstanceNum = 0;
        static public Dictionary<string, Action<string>> events = new Dictionary<string, Action<string>>();

        private const bool ON = true;//1220
        private const bool OFF = false;//1220

        static public void loadJson(MainForm form)
        {
            mainForm = form;
            var jsonFileTxt = File.ReadAllText(MainForm.RootPath + @"\data\ErrorHandling.json");
            errors = JsonConvert.DeserializeObject<List<Errors>>(jsonFileTxt);
            SerialCommunication.addEventList("000E", readErrorData);
            SerialCommunication.addEventList("000C", readErrorData);

            setEventList();
        }

        static private void setEventList()
        {
            foreach (var error in errors)
            {
                events.Add(error.ErrNum, showErrorForm);
            }

            events["0008"] += emergencyReset;
        }

        static private void readErrorData(string dummy)
        {
            SerialCommunication.writeByteSirialPort("WR", "D0001", 8, "", checkErrorState);
        }

        static private void checkErrorState(string data)
        {
            try
            {
                for (var i = 0; i < 8; i++)
                {
                    var str = data.Substring(i * 4, 4);
                    var num = Convert.ToInt32(str, 16);
                    var key = (i + 1).ToString().PadLeft(4, '0');
                    if (num > 0)
                        events[key](key);
                    else
                        delErrorForm(key);

                }
            }
            catch(ArgumentOutOfRangeException e)
            {
                readErrorData("");
            }
            
        }

        static private void emergencyReset(string dummy)
        {
            ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, OFF);
            ControlOrder.orderExecute(ControlOrder.OrderName.Not_Clamp, OFF);
            ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, OFF);

            MainForm.seriarReadEvent = mainForm.emergencyReset;
            mainForm.invorkFunction("");
        }

        static private void showErrorForm(string errNum)
        {
            if(!errorForm.ContainsKey(errNum))
            {
                var e = errors.Find(err => err.ErrNum.Equals(errNum));
                errorForm.Add(errNum, new ErrorForm(e));
                errorForm[errNum].Show();

                if (formInstanceNum++ == 0)
                {
                    mainForm.Enabled = false;
                    ControlOrder.orderExecute(ControlOrder.OrderName.Error_Complete, ON);
                }
                    
            }
        }

        static private void delErrorForm(string resolveNum)
        {
            var e = (Errors)errors.Find(err => err.ErrNum.Equals(resolveNum));

            if(errorForm.ContainsKey(e.ErrNum))
                errorForm[e.ErrNum].errReleaseFlag = true;
        }


        static public void removeFormInstance(string errNum)
        {
            errorForm.Remove(errNum);
            if (--formInstanceNum == 0)
            {
                mainForm.Enabled = true;
                ControlOrder.orderExecute(ControlOrder.OrderName.Error_Complete, OFF);
            }
                
        }
    }

    class Errors
    {
        public string ErrNum { get; set; }
        public string Msg { get; set; }
        public string ResolveNum { get; set; }
        public string Option { get; set; }
    }
}
