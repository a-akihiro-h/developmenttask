﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentTask
{
    static class MyUtility
    {
        static public Image creatGraphicImage(Bitmap src, int widht, int height)
        {
            //描画先とするImageオブジェクトを作成する
            Bitmap canvas;
            //ImageオブジェクトのGraphicsオブジェクトを作成する
            Graphics g;

            int dstWidth, dstHeight;

            if (((double)widht / src.Width) <= ((double)height / src.Height))
            {
                dstWidth = widht;
                dstHeight = Convert.ToInt32(src.Height * ((double)widht / src.Width));
            }
            else
            {
                dstWidth = Convert.ToInt32(src.Width * ((double)height / src.Height));
                dstHeight = height;
            }

            canvas = new Bitmap(dstWidth, dstHeight);
            g = Graphics.FromImage(canvas);

            //画像のサイズを変更してcanvasに描画する
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(src, 0, 0, dstWidth, dstHeight);
            g.Dispose();

            return canvas;
        }

        static public Image creatGraphicImage(Bitmap src, double scale)
        {
            var w = Convert.ToInt32(src.Width * scale);
            var h = Convert.ToInt32(src.Height * scale);
            //描画先とするImageオブジェクトを作成する
            Bitmap canvas = new Bitmap(w, h);
            //ImageオブジェクトのGraphicsオブジェクトを作成する
            Graphics g = Graphics.FromImage(canvas);


            //画像のサイズを変更してcanvasに描画する
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.DrawImage(src, 0, 0, w, h);
            g.Dispose();

            return canvas;
        }

        static public PointF calcCenterPoint(dynamic parent, dynamic child)
        {
            var w = parent.Width / 2.0F - child.Width / 2.0F;
            var h = parent.Height / 2.0F - child.Height / 2.0F;

            return new PointF(w, h);
        }

        static public PointF calcCenterPoint(dynamic parent, dynamic child, float offset_W, float offset_H)
        {
            var w = parent.Width / 2.0F - child.Width / 2.0F + offset_W;
            var h = parent.Height / 2.0F - child.Height / 2.0F + offset_H;

            return new PointF(w, h);
        }

    }
}
