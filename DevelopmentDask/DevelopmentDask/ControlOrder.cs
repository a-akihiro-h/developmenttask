﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevelopmentTask
{
    class ControlOrder
    {
        static private string[] operateOrderTable =
        {
            "0000", "0001", "0002", "0003",
            "0071", "0072", "0073", "0074",
            "0075", "0076", "0077", "0078",
            "0079", "0080", "0081", "0100",
            "0101", "0300", "0301"
        };

        static private string[] stepOrderTable =
        {
            "0000", "0068", "0069", "0070",
            "0071", "0072", "0073", "0074",
            "0075", "0076", "0077", "0078",
            "0079", "0080", "0081", "0100",
            "0101", "0300", "0301"
        };

        public enum OrderName
        {   Preparation,        Stocker_L,          Stocker_R,          Sec_Stoker,
            Bag_Supply,         Bag_Rotate,         Product_Discharge,  Suction_Inside_R,
            Suction_Inside_L,   Suction_Outside_R,  Suction_Outside_L,  Suction_Bag_R,
            Suction_Bag_L,      Bag_Support,        Bag_Keep,           Start,
            Error_Complete,     Clamp,              Not_Clamp
        };

        static public void orderExecute(OrderName order, bool state, bool step = false)
        {
            var stateStr = (state) ? "1" : "0";
            var index = order;
            var orderStr = "M";
            orderStr += (step) ? stepOrderTable[(int)order] : operateOrderTable[(int)order];
            SerialCommunication.writeByteSirialPort("BW", orderStr, 1, stateStr, null);

            countUpClampCount(order, state, step);
        }

        static private void countUpClampCount(OrderName order, bool state, bool step)
        {
            if (order == OrderName.Clamp && state == true)
                MainForm.ClampCount++;
            else if(order == OrderName.Sec_Stoker && state == true && step == false)
                MainForm.ClampCount++;
        }
    }
}