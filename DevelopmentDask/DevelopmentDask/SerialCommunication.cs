﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;


namespace DevelopmentTask
{
    class SerialCommunication
    {
        static string DefaultPortName = "COM4";
        static List<string> PortNameList;
        static int BaudRate = 9600;
        static Parity Parity = Parity.None;
        static int DataBits = 8;
        static StopBits StopBits = StopBits.One;
        static object lockObject = new object();
        static string _String = "";

        private const bool ON = true;//1220
        private const bool OFF = false;//1220
        private const string checkerStr = "AAAAA";

        static private SerialPort serialPort;
        static public Action<string> readEventHandle;
        static public Dictionary<string, Action<string>> EventList = new Dictionary<string, Action<string>>();
        static public bool isConnected = false;

        static public  bool open()
        {
            if (!isConnected)
            {
                PortNameList = SerialPort.GetPortNames().ToList();
                PortNameList.Remove(DefaultPortName);
                PortNameList.Insert(0, DefaultPortName);
                return openSerialPort();
            }

            return false;
        }

        static private void checkedConnected(string str)
        {
            var _str = str.Split('5');
            if (_str.Length < 2)
                return;
            if (_str[1].Equals(checkerStr))
                isConnected = true;
        }

        static private bool openSerialPort()
        {
            foreach (var name in PortNameList)
            {
                try
                {
                    serialPort = new SerialPort(name, BaudRate, Parity, DataBits, StopBits);
                    serialPort.Open();
                    serialPort.WriteTimeout = 1000;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(portReadHandle);
                    SerialCommunication.writeByteSirialPort("TT", "", 5, "AAAAA", checkedConnected);

                    Thread.Sleep(2000);
                    if (isConnected)
                    {
                        return true;
                    }
                }
                catch(Exception e)
                {
                }

                closeSerialPort();
            }
            
            return false;
        }

        static public void closeSerialPort()
        {
            if(serialPort == null)
                return;

            if (serialPort.IsOpen)
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
                serialPort.Close();
                serialPort.Dispose();
            }
            
        }

        static public void addEventList(string key, Action<string> action)
        {
            if (EventList.Keys.Contains(key))
                EventList[key] += action;
            else
                EventList.Add(key, action);
        }
        static public void dellEventList(string key, Action<string> action)
        {
            if (EventList.Keys.Contains(key))
                EventList[key] -= action;
        }

        static private void portReadHandle(object sender, SerialDataReceivedEventArgs e)
        {
            int rbyte = serialPort.BytesToRead;
            byte[] buffer = new byte[rbyte];
            int read = 0;
            var str = "";

            while (read < rbyte)
            {
                int length = serialPort.Read(buffer, read, rbyte - read);
                read += length;
            }

            if (buffer[0] == (byte)0x06 || buffer[0] == (byte)0x02)//制御文字ENQ判定
                _String = "";

            str = Encoding.UTF8.GetString(buffer);
            if (str.Equals("$00FF"))
                return;

            _String += str;

            if (buffer[buffer.Length - 1] != (byte)0x03)
                return;
            else
                _String = _String.Substring(1, _String.Length - 2);

            
            if (0 <= _String.IndexOf("FE"))
            {
                string[] s = { "FE" };
                str = _String.Split(s, StringSplitOptions.None)[1].ToString().Substring(0, 4);
            }
            else if(0 <= _String.IndexOf("FF"))
            {
                string[] s = { "FF" };
                var tmp = _String.Split(s, StringSplitOptions.None)[1].ToString();
                readEventHandle(tmp);
                return;
            }

            if (EventList.ContainsKey(str) && EventList[str] != null)
            {
                MainForm.seriarReadEvent = EventList[str];
                readEventHandle(str);
            }
            
        }

        static public void writeByteSirialPort(string mode, string fstDevChar, uint devPieces, string ctlCmd = "", Action<string> action = null)
        {
            string cmdStr = fstDevChar + devPieces.ToString().PadLeft(2, '0') + ctlCmd;
            MainForm.seriarReadEvent = action; 
            Task.Run(() => writeSirialPort(mode, cmdStr));
        }

        static public bool writeSirialPort(string writeType, string cmd)
        {
            lock (lockObject)
            {
                try
                {
                    if (!serialPort.IsOpen)
                        return false;
                    if (String.IsNullOrEmpty(cmd))
                        return false;

                    string cmdStr = "00FF" + writeType + "0" + cmd;
                    uint checkSum = 0;

                    foreach (var ch in cmdStr)
                        checkSum += ch;

                    char[] checkSumHex = Convert.ToString(checkSum, 16).ToCharArray();
                    var i = checkSumHex.Count() - 1;

                    cmdStr = Convert.ToChar(0x05) + cmdStr + checkSumHex[i - 1] + checkSumHex[i];

                    
                    Thread.Sleep(200);                        

                    serialPort.Write(cmdStr);
                    return true;
                }
                catch (TimeoutException e)
                {
                    MessageBox.Show("write time out");
                    return false;
                }

            }
        }

    }
}
