﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace DevelopmentTask
{
    public partial class ProductSelectionPage : UserControl
    {
        public SettingPage NextPage;
        public HistoryPage WorkHistoryPage;
        public InspectionPage InspectionPage_;
        private event EventHandler exit_Click;
        private bool lockFlag = false;

        public ProductSelectionPage()
        {
            InitializeComponent();

            listBox1.SelectedValueChanged += workerSelect;
            this.Visible = true;
        }

        private void ProductSelectionPage_Load(object sender, EventArgs e)
        {

            WorkerList = readWorkerNameFile();
            if (WorkerList != null)
            {
                foreach (var tmp in WorkerList)
                {
                    addListBox(listBox1.Items.Count, tmp);
                }
            }
            
        }

        public event EventHandler Exit_Click
        {
            add
            {
                if (value != null)
                    exit_Click += value;
            }
            remove
            {
                if (value != null)
                    exit_Click -= value;
            }
        }

        public string WorkerName { get; set; } = "";
        public string ProductType { get; set; } = "";
        public List<string> WorkerList { get; set; }


        private async void productTypeButton_Click(object sender, EventArgs e)
        {
            var name = listBox1.SelectedItem;
            bool result = false;

            if (lockFlag) return;

            if(name == null)
            {
                MessageBoxSet.Show("作業者を選択してください");
                return;
            }

            if (!SerialCommunication.isConnected)
            {
                lockFlag = true;
                result = await Task.Run(() => SerialCommunication.open());

            }

            lockFlag = false;
            if (result || SerialCommunication.isConnected)
            {
                Button btn = (Button)sender;
                ProductType = btn.Text;
                WorkerName = name.ToString();

                this.Visible = false;
                NextPage.Visible = true;
                return;
            }
            MessageBoxSet.Show("シリアルポートを開くことができません\n\nケーブルが装置と接続されているか確認してください");

        }

        private async void inspectionButton_Click(object sender, EventArgs e)
        {
            bool result = false;

            if (lockFlag) return;

            if (!SerialCommunication.isConnected)
            {
                lockFlag = true;
                result = await Task.Run(() => SerialCommunication.open());
            }

            lockFlag = false;
            if (result || SerialCommunication.isConnected)
            {
                this.Visible = false;
                InspectionPage_.BackPage = this;
                InspectionPage_.Visible = true;
                ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, true);//M0000信号ON0111
                return;
            }
            MessageBoxSet.Show("シリアルポートを開くことができません\n\nケーブルが装置と接続されているか確認してください");

        }

        private void nameAddButton_Click(object sender, EventArgs e)
        {
            AddNameForm addNameForm = new AddNameForm();
            var result = addNameForm.ShowDialog();

            if (result == DialogResult.Yes && addNameForm.AddName != null)
            {
                addListBox(listBox1.Items.Count, addNameForm.AddName);
                writeWorkerNameFile(listBox1.Items);
                WorkerList.Add(addNameForm.AddName);
            }

            addNameForm.Close();
        }

        private void nameDellButton_Click(object sender, EventArgs e)
        {
            DeleteNameForm delNameForm = new DeleteNameForm();
            delNameForm.Name_List(listBox1);
            var result = delNameForm.ShowDialog();

            if (result == DialogResult.Yes)
            {
                foreach (String name in delNameForm.delNames)
                {
                    dellListBox(name);
                    WorkerList.Remove(name);
                }

                writeWorkerNameFile(listBox1.Items);
            }

            delNameForm.Close();
        }

        private void endButton_Click(object sender, EventArgs e)
        {
            exit_Click(sender, e);
        }

        private void workHistoryButton_Click(object sender, EventArgs e)
        {
            WorkHistoryPage.Visible = true;
            this.Visible = false;
        }

        private void workerSelect(object sender, EventArgs e)
        {
            string itemName;

            try
            {
                itemName = listBox1.SelectedItem.ToString();
            }
            catch (NullReferenceException err)
            {
                return;
            }
        }
        

        private List<string> readWorkerNameFile()
        {
            using (StreamReader workerName = new StreamReader(MainForm.RootPath + @"\data\name.data"))
            {
                string line;
                List<string> list = new List<string>();
                int count = 0;

                while ((line = workerName.ReadLine()) != null)
                {
                    list.Add(line);
                    count++;
                }

                if (count > 0)
                    return list;
                else
                    return null;

            }
        }

        private void writeWorkerNameFile(ListBox.ObjectCollection name)
        {
            using (StreamWriter workerName = new StreamWriter(MainForm.RootPath + @"\data\name.data"))
            {
                foreach (var tmp in name)
                {
                    workerName.WriteLine(tmp);
                }
            }
        }

        private bool addListBox(int index, string name)
        {
            if (listBox1.Items.Contains(name))
                return false;

            listBox1.Items.Insert(index, name);
            return true;
        }

        private bool dellListBox(string name)
        {
            if (listBox1.Items.Contains(name) == false)
                return false;

            listBox1.Items.Remove(name);
            return true;
        }

        
    }
}
