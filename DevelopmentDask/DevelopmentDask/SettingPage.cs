﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace DevelopmentTask
{
    public partial class SettingPage : UserControl
    {
        public ProductSelectionPage PreviousPage;
        public OperationPage NextPage;

        public string WokerName { get; set; } = "";
        private Form waitingForm;
        private bool ready = false;

        private const bool ON = true;//1217
        private const bool OFF = false;//1217

        System.Timers.Timer startSignalOffTimer = new System.Timers.Timer();
        Timer timeOutWaitFormTimer = new Timer();

        public SettingPage()
        {
            InitializeComponent();

            tenKey1.OnClickedTenKeyButtonEventHandler += new EventHandler(this.TenKey_Button_Click);            
            this.Visible = false;
            this.VisibleChanged += this.visibleChangeEvent;
            waitingForm = new Form();

            //タイマー追加0111
            startSignalOffTimer.Interval = 1000;
            timeOutWaitFormTimer.Interval = 5000;

            startSignalOffTimer.Elapsed += (sender, e) =>
            {
                ControlOrder.orderExecute(ControlOrder.OrderName.Start, OFF);
                startSignalOffTimer.Stop();
            };

            timeOutWaitFormTimer.Tick += timeOutWaitForm;
        }

        private void SettingPage_Load(object sender, EventArgs e)
        {
            
        }

        private void TenKey_Button_Click(object sender, EventArgs e)
        {
            int cursorPoint = textBox1.SelectionStart;
            string str;

            textBox1.Focus();

            if (tenKey1.ClickedButtonType.Equals("Del"))
                textBox1.Text = "";
            else
            {
                str = textBox1.Text.Insert(cursorPoint, tenKey1.ClickedButtonType);
                textBox1.Text = str;
                textBox1.SelectionStart = cursorPoint + 1;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            this.PreviousPage.Visible = true;
            ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, OFF);//M0000信号をOFF1219
        }

        private Boolean workNumCheck()
        {
            if (String.IsNullOrEmpty(this.textBox1.Text))
            {
                MessageBoxSet.Show("ゴムの数値を入力してください");
                return false;
            }

            int workNum = Convert.ToInt32(this.textBox1.Text);
            if (workNum <= 0)
            {
                MessageBoxSet.Show("1以上の数値を入力してください");
                return false;
            }

            return true;
        }

        private void visibleChangeEvent(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.textBox1.Text = "100";
                this.label4.Text = PreviousPage.WorkerName;
                this.label5.Text = PreviousPage.ProductType;

                textBox1.SelectionStart = textBox1.Text.Length;
                this.ActiveControl = textBox1;

                operationButton.Text = "運転準備";

                SerialCommunication.addEventList("0000", serialPortReadEvent);
            }
            else
            {
                SerialCommunication.dellEventList("0000", serialPortReadEvent);
            }
            
        }

        private void initWaitingForm()
        {
            creatWaitForm();
            creatPictureBox();
            creatLabel();

            waitingForm.Load += (sender2, e2) =>
            {
                var point = MyUtility.calcCenterPoint(waitingForm, waitingForm.Controls["label"]);
                waitingForm.Controls["label"].Location = Point.Round(point);
            };
            
        }

        private void creatWaitForm()
        {
            waitingForm = (waitingForm.IsDisposed) ? new Form() : waitingForm;

            Screen s = Screen.FromControl(this);
            waitingForm.Size = new Size((int)(s.Bounds.Width * 0.6), (int)(s.Bounds.Height * 0.6));
            waitingForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            waitingForm.StartPosition = FormStartPosition.CenterScreen;
            waitingForm.FormClosing += this.waitFormExit;
            waitingForm.LostFocus += (sender, e) =>
            {
                waitingForm.Focus();
            };
        }
        private void creatPictureBox()
        {
            PictureBox pictureBox = new PictureBox();
            pictureBox.SuspendLayout();
            pictureBox.Image = Properties.Resources.loader;
            pictureBox.Name = "pictureBox";
            pictureBox.Width = pictureBox.Image.Width;
            pictureBox.Height = pictureBox.Image.Height;
            waitingForm.Controls.Add(pictureBox);

            var x = (int)(waitingForm.Width * 0.7 - pictureBox.Width / 2.0);
            var y = (int)(waitingForm.Height / 2.0 - pictureBox.Height / 2.0);
            waitingForm.Controls["pictureBox"].Location = new Point(x, y);
        }
        private void creatLabel()
        {
            Label label = new Label();
            label.SuspendLayout();
            label.Name = "label";
            label.Font = new Font("MS UI Gothic", 40);
            label.AutoSize = true;
            label.Text = "運転準備中";

            waitingForm.Controls.Add(label);
        }

        private void operationButton_Click(object sender, EventArgs e)
        {
           var check = workNumCheck();
           if (!check) return;

            //運転準備、運転開始ボタンの切り替え実装1218
            if(operationButton.Text == "運転準備")
            {
                if(waitingForm.IsDisposed || !waitingForm.Created)
                    initWaitingForm();

                this.Enabled = false;
                timeOutWaitFormTimer.Start();
                waitingForm.Show();

                if (ready)
                {
                    closeWaitForm("準備完了", true);
                    return;
                }

                //PLCと通信し、運転準備接点をONにする。         
                ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, ON);//運転準備信号ON1217
            }
            else
            {
                ControlOrder.orderExecute(ControlOrder.OrderName.Start, ON);//運転開始信号ON1220
                startSignalOffTimer.Start();
                NextPage.WorkTargetNum = Convert.ToInt32(this.textBox1.Text);
                this.Visible = false;
                NextPage.Visible = true;
            }

        }

        private void serialPortReadEvent(string str)
        {
            if (str != "0000")
                return;

            if (waitingForm.IsDisposed || !waitingForm.Created)
            {
                ready = true;
                return;
            }


            closeWaitForm("準備完了", true);
            
        }

        private void closeWaitForm(string msg, bool _ready)
        {
            Timer timer = new Timer();
            timer.Interval = 3000;

            timeOutWaitFormTimer.Stop();

            waitingForm.Controls["pictureBox"].Visible = false;

            var x = (int)(waitingForm.Width * 0.15);
            var y = (int)(waitingForm.Height / 2.0 - waitingForm.Controls["label"].Height / 2.0);
            waitingForm.Controls["label"].Location = new Point(x, y);

            waitingForm.Controls["label"].Text = msg;

            timer.Tick += (sender, e) => {
                timer.Stop();
                waitingForm.FormClosing -= this.waitFormExit;
                waitingForm.Close();
                waitingForm.Dispose();
                if(_ready)
                    operationButton.Text = "運転開始";
                this.Enabled = true;
                ready = false;
            };

            timer.Start();
        }

        private void timeOutWaitForm(object sender, EventArgs e)
        {
            closeWaitForm("接続がタイムアウトしました\nシリアルケーブルを確認してください", false);
        }

        private void waitFormExit(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

    }
}
