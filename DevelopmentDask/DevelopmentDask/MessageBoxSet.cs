using System;
using System.Collections.Generic;
using System.Text;

namespace DevelopmentTask
{
    class MessageBoxSet : NonMsgNet.MsgBoxClass
    {
        public static Boolean NonMsgNet_Init()
        {
            // シェアウェア用ライセンスキー(未使用)
            ////NonMsgNet.MsgBoxClass.LicenseKey = "ここにライセンスキーを設定してください";

            // メッセージフォント名("ＭＳ Ｐゴシック","ＭＳ Ｐ明朝","ＭＳ ゴシック","ＭＳ 明朝")
            NonMsgNet.MsgBoxClass.MsgFontName = "ＭＳ Ｐゴシック";
            // メッセージフォントサイズ
            NonMsgNet.MsgBoxClass.MsgFontSize = 30;
            // メッセージフォント太文字
            NonMsgNet.MsgBoxClass.MsgFontBold = false;
            // メッセージフォント斜体
            NonMsgNet.MsgBoxClass.MsgFontItalic = false;
            // メッセージフォント取消線
            NonMsgNet.MsgBoxClass.MsgFontStrikethru = false;
            // メッセージフォント下線
            NonMsgNet.MsgBoxClass.MsgFontUnderline = false;
            // メッセージ幅微調整1(0.9〜(1.02)〜1.1)
            NonMsgNet.MsgBoxClass.MsgWidthP1 = 1.02;
            // メッセージ幅微調整2(-50〜(+5.0)〜+50)
            NonMsgNet.MsgBoxClass.MsgWidthP2 = 5.0;
            // メッセージ高微調整1(0.9〜(1.02)〜1.1)
            NonMsgNet.MsgBoxClass.MsgHeightP1 = 1.02;
            // メッセージ高微調整2(-50〜(+5.0)〜+50)
            NonMsgNet.MsgBoxClass.MsgHeightP2 = 5.0;

            // ボタンフォント名("ＭＳ Ｐゴシック","ＭＳ Ｐ明朝","ＭＳ ゴシック","ＭＳ 明朝")
            NonMsgNet.MsgBoxClass.BtnFontName = "ＭＳ Ｐゴシック";
            // ボタンフォントサイズ
            NonMsgNet.MsgBoxClass.BtnFontSize = 30;
            // ボタンフォント太文字
            NonMsgNet.MsgBoxClass.BtnFontBold = false;
            // ボタンフォント斜体
            NonMsgNet.MsgBoxClass.BtnFontItalic = false;
            // ボタンフォント取消線
            NonMsgNet.MsgBoxClass.BtnFontStrikethru = false;
            // ボタンフォント下線
            NonMsgNet.MsgBoxClass.BtnFontUnderline = false;
            // ボタン幅微調整1(0.9〜(1.02)〜1.1)
            NonMsgNet.MsgBoxClass.BtnWidthP1 = 1.02;
            // ボタン幅微調整2(-50〜(+5.0)〜+50)
            NonMsgNet.MsgBoxClass.BtnWidthP2 = 5.0;
            // ボタン高微調整1(0.9〜(1.02)〜1.1)
            NonMsgNet.MsgBoxClass.BtnHeightP1 = 1.02;
            // ボタン高微調整2(-50〜(+5.0)〜+50)
            NonMsgNet.MsgBoxClass.BtnHeightP2 = 5.0;

            // メッセージボックス表示位置(CenterScree=False,CenterParen=True)
            NonMsgNet.MsgBoxClass.StartPosition = false;

            // NonMsgNet有効
            NonMsgNet.MsgBoxClass.Enable = true;
            return NonMsgNet.MsgBoxClass.Enable;
        }
    }
}
