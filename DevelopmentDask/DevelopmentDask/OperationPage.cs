﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using DevelopmentTask;

namespace DevelopmentTask
{
    public partial class OperationPage : UserControl
    {
        private int workTargetNum;
        private int leftStockerNum;
        private int rightStockerNum;
        private int plcCount_R, plcCount_L;
        private DateTime startDate;
        private TimeSpan workingTime;
        private const bool ON   = true;
        private const bool OFF  = false;

        private int finishedProductNum { get; set; } = 0;

        public int LeftStockerDiff { get; set; } = 0;
        public int RightStockerDiff { get; set; } = 0;
        

        System.Timers.Timer rightStockerCloseTimer = new System.Timers.Timer();
        System.Timers.Timer leftStockerCloseTimer = new System.Timers.Timer();
        System.Timers.Timer secondStockerCloseTimer = new System.Timers.Timer();
        System.Timers.Timer clampSignalOffTimer = new System.Timers.Timer();
        System.Timers.Timer notClampSignalOffTimer = new System.Timers.Timer();
        System.Timers.Timer timeUpdateTimer = new System.Timers.Timer();

        private enum PHASE {STOCKER_OPEN1, STOCKER_OPEN2, CLAMP};
        private enum PARITY {EVEN, ODD};
        private enum ScreenTransition { INIT, CHANGE, EXIT};
        PHASE   phase;
        PARITY parity;
        ScreenTransition screenTransition;

        public ProductSelectionPage productSelection;
        public InspectionPage manualPage;

        public OperationPage()
        {
            InitializeComponent();

            this.Visible = false;
            this.VisibleChanged += VisibleChangedEvent;

            workTargetNum   = 100;
            leftStockerNum  = 0;
            rightStockerNum = 0;
            plcCount_L = plcCount_R = 0;
            phase = PHASE.STOCKER_OPEN1;
            screenTransition = ScreenTransition.INIT;

            this.stockerDiaglam1.ImageScale = 0.9F;


            rightStockerCloseTimer.Interval = 1000;
            leftStockerCloseTimer.Interval = 1000;
            secondStockerCloseTimer.Interval = 1500;
            clampSignalOffTimer.Interval = 1000;
            notClampSignalOffTimer.Interval = 1000;
            timeUpdateTimer.Interval = 1000;

            rightStockerCloseTimer.Elapsed += (sender, e) =>
            {
                ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_R, OFF);
                rightStockerCloseTimer.Stop();
            };

            leftStockerCloseTimer.Elapsed += (sender, e) =>
            {
                leftStockerCloseTimer.Stop();
                ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_L, OFF);               
            };

            secondStockerCloseTimer.Elapsed += (sender, e) =>
            {
                secondStockerCloseTimer.Stop();
                ControlOrder.orderExecute(ControlOrder.OrderName.Sec_Stoker, OFF);        
            };

            clampSignalOffTimer.Elapsed += (sender, e) =>
            {
                clampSignalOffTimer.Stop();
                ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, OFF);       
            };

            notClampSignalOffTimer.Elapsed += (sender, e) =>
            {
                notClampSignalOffTimer.Stop();
                ControlOrder.orderExecute(ControlOrder.OrderName.Not_Clamp, OFF);
            };

            timeUpdateTimer.Elapsed += updateClock;
            
        }

        private void OperationPage_Load (object sender, EventArgs e)
        {
            Error.events["0008"] += emergencyReset;
        }

        private void emergencyReset(string dummy)
        {
            screenTransition = ScreenTransition.EXIT;
            VisibleChangedEvent(null, null);
            screenTransition = ScreenTransition.INIT;
            phase = PHASE.STOCKER_OPEN1;
            
        }

        private void readNum(string dummy)
        {
            SerialCommunication.writeByteSirialPort("WR", "D0300", 2, "", convertNum);
        }

        private void cycleExit(string dummy)
        {
            if (this.Visible)
            {
                ControlOrder.orderExecute(ControlOrder.OrderName.Sec_Stoker, OFF);   
            }
            else
            {
                ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, OFF);
                ControlOrder.orderExecute(ControlOrder.OrderName.Not_Clamp, OFF);
                ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, OFF);
            }
        }

        private void convertNum(string str)
        {
            RightStockerNum = plcCount_R = Convert.ToInt32(str.Substring(0, 4), 16) + RightStockerDiff;
            LeftStockerNum = plcCount_L = Convert.ToInt32(str.Substring(4, 4), 16)  + LeftStockerDiff;
            checkWorkNum();
        }

        public int WorkTargetNum
        {
            get { return workTargetNum; }

            set
            {
                if (value < 0)
                {
                    MessageBoxSet.Show("err: workTargetNumの値が負");
                }
                else
                {
                    workTargetNum = value;
                    parity = (value % 2 == 0) ? PARITY.EVEN : PARITY.ODD;
                }
            }
        }
        public int LeftStockerNum
        {
            get { return leftStockerNum; }

            set
            {
                if (value < 0)
                {
                    MessageBoxSet.Show("err: leftStockerNumの値が負");
                }
                else
                {
                    leftStockerNum = value;               
                }
            }
        }
        public int RightStockerNum
        {
            get { return rightStockerNum; }

            set
            {
                if (value < 0)
                {
                    MessageBoxSet.Show("err: rightStockerNumの値が負");
                }
                else
                {
                    rightStockerNum = value;
                }
            }
        }

        public int SecondStockerNum { get; set; } = 0;

        private void updateClock(object sender, EventArgs e)
        {
            var time = DateTime.Now;
            var now = time - startDate;

            Action action = () => { this.nowTimeLabel.Text = String.Format("{0:00}時 {1:00}分 {2:00}秒", time.Hour, time.Minute, time.Second); };
            Invoke(action);

            action = () => { this.passingTimeLabel.Text = String.Format("{0:00}:{1:00}:{2:00}", now.TotalHours, now.Minutes, now.Seconds); };
            Invoke(action);
        }

        private void checkWorkNum()
        {
            switch (phase)
            {
                case PHASE.STOCKER_OPEN1:
                    if (rightStockerNum >= Math.Truncate(WorkTargetNum / 2.0))
                    {
                        SecondStockerNum += rightStockerNum;
                        //右1次ストッカを開く
                        ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_R, ON);
                        rightStockerCloseTimer.Start();

                        phase = PHASE.STOCKER_OPEN2;
                        rightStockerNum = 0;
                        RightStockerDiff = 0;
                        plcCount_R = 0;
                    }
                    if ((leftStockerNum >= Math.Truncate(WorkTargetNum / 2.0)) && (parity == PARITY.EVEN || SecondStockerNum == 0))
                    {
                        SecondStockerNum += leftStockerNum;
                        //左1次ストッカを開く
                        ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_L, ON);
                        leftStockerCloseTimer.Start();

                        phase = (phase == PHASE.STOCKER_OPEN2) ? PHASE.CLAMP : PHASE.STOCKER_OPEN2;
                        leftStockerNum = 0;
                        LeftStockerDiff = 0;
                        plcCount_L = 0;
                    }

                    if (phase == PHASE.CLAMP)
                        checkWorkNum();

                    updateNumText();
                    break;

                case PHASE.STOCKER_OPEN2:
                    if (rightStockerNum >= (WorkTargetNum - SecondStockerNum))
                    {
                        SecondStockerNum += rightStockerNum;
                        //右1次ストッカを開く
                        ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_R, ON);
                        rightStockerCloseTimer.Start();

                        phase = PHASE.CLAMP;
                        rightStockerNum = 0;
                        RightStockerDiff = 0;
                        plcCount_R = 0;
                        checkWorkNum();
                    }
                    else if (leftStockerNum >= (WorkTargetNum - SecondStockerNum))
                    {
                        SecondStockerNum += leftStockerNum;
                        //左1次ストッカを開く
                        ControlOrder.orderExecute(ControlOrder.OrderName.Stocker_L, ON);
                        leftStockerCloseTimer.Start();

                        phase = PHASE.CLAMP;
                        leftStockerNum = 0;
                        LeftStockerDiff = 0;
                        plcCount_L = 0;
                        checkWorkNum();
                    }

                    updateNumText();
                    break;

                case PHASE.CLAMP:
                    //袋をセットして2次ストッカを開き圧着開始
                    ControlOrder.orderExecute(ControlOrder.OrderName.Sec_Stoker, ON);
                    phase = PHASE.STOCKER_OPEN1;
                    finishedProductNum++;
                    SecondStockerNum = 0;

                    updateNumText();
                    break;
            }
        }

        private void updateNumText()
        {
            this.stockerDiaglam1.rightStockerTxt = rightStockerNum.ToString();
            this.stockerDiaglam1.leftStockerTxt = leftStockerNum.ToString();
            this.stockerDiaglam1.secondStockerTxt = SecondStockerNum.ToString();
            this.finishedProductNumLabel.Text = finishedProductNum.ToString();
        }

        private void manualButton_Click(object sender, EventArgs e)
        {
            manualPage.BackPage = this;
            this.Visible = false;
            manualPage.Visible = true;
        }

        private void initNum()
        {
            leftStockerNum = 0;
            rightStockerNum = 0;
            LeftStockerDiff = 0;
            RightStockerDiff = 0;
            SecondStockerNum = 0;
            finishedProductNum = 0;
            plcCount_L = 0;
            plcCount_R = 0;
        }

        private void VisibleChangedEvent(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                if(screenTransition == ScreenTransition.INIT)
                {
                    startDate = DateTime.Now;
                    updateClock(sender, e);
                    timeUpdateTimer.Start();
                    screenTransition = ScreenTransition.CHANGE;
                }
                SerialCommunication.addEventList("00A1", cycleExit);
                SerialCommunication.addEventList("0001", readNum);
                checkWorkNum();
            }
            else if(screenTransition == ScreenTransition.EXIT)
            {
                if (startDate == null)
                    return;

                try
                {
                    DateTime end = DateTime.Now;
                    workingTime = (end - startDate);

                    string time = String.Format("{0:00}:{1:#00}:{2:#00}", workingTime.Hours, workingTime.Minutes, workingTime.Seconds);
                    string date = String.Format("{0}/{1}", startDate.Month, startDate.Day);

                    var num = (finishedProductNum * workTargetNum) + leftStockerNum + rightStockerNum + SecondStockerNum;

                    List<string> workHistory = new List<string>{
                        date,
                        productSelection.ProductType,
                        productSelection.WorkerName,
                        time,
                        num.ToString(),
                        finishedProductNum.ToString()
                    };

                    writeWorkerNameFile(workHistory);

                    timeUpdateTimer.Stop();
                    initNum();
                    screenTransition = ScreenTransition.INIT;
                    SerialCommunication.dellEventList("00A1", cycleExit);
                    SerialCommunication.dellEventList("0001", readNum);

                }
                catch(Exception ex)
                {
                    MessageBoxSet.Show("エラー", "ERR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MessageBoxSet.Show(ex.ToString());
                }
            }
            else
            {
                SerialCommunication.dellEventList("00A1", cycleExit);
                SerialCommunication.dellEventList("0001", readNum);
            }

        }

        private void EndWorkButton_Click(object sender, EventArgs e)
        {
            if (leftStockerNum + rightStockerNum + SecondStockerNum > 0)
            {
                var msg =   "製品がストッカに存在します\n" +
                            "圧着しますか?\n\n" +
                            "はい\t:  ストッカに残っているワークを圧着して搬出します\n" +
                            "いいえ\t:  ストッカに残っているワークを圧着せずに搬出します";

                var result = MessageBoxSet.Show(msg, "確認", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                
                if (result == DialogResult.Yes)
                {
                    ControlOrder.orderExecute(ControlOrder.OrderName.Clamp, ON);
                } 
                else if (result == DialogResult.No)
                {
                    ControlOrder.orderExecute(ControlOrder.OrderName.Not_Clamp, ON);//Sec_stoker→Not_Clamp,OFF→ONに変更1218
                }

                if(result != DialogResult.Cancel)
                {
                    screenTransition = ScreenTransition.EXIT;
                    this.Visible = false;
                    productSelection.Visible = true;
                }
                
            }
            else
            {

                var result = MessageBoxSet.Show("作業を終了してよろしいですか?", "確認", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    screenTransition = ScreenTransition.EXIT;
                    this.Visible = false;
                    productSelection.Visible = true;
                    ControlOrder.orderExecute(ControlOrder.OrderName.Preparation, OFF);
                }
            }  
        }

        private void writeWorkerNameFile(List<string> data)
        {
            string directory = String.Format(MainForm.RootPath + @"\data\history");
            string path = String.Format(@"{0}\{1}.csv", directory, startDate.Year);
            SafeCreateDirectory(directory);

            using (StreamWriter workerName = new StreamWriter(path, true))
            {
                string history = "";
                foreach (var tmp in data)
                {
                    history += tmp + ",";
                }
                
                workerName.WriteLine( history.Remove( history.LastIndexOf(",") ) );
            }
        }


        private DirectoryInfo SafeCreateDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                return null;
            }
            return Directory.CreateDirectory(path);
        }
    }
}
