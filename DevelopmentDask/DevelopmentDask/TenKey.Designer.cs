﻿namespace DevelopmentTask
{
    partial class TenKey
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tenKey_8 = new System.Windows.Forms.Button();
            this.tenKey_9 = new System.Windows.Forms.Button();
            this.tenKey_4 = new System.Windows.Forms.Button();
            this.tenKey_5 = new System.Windows.Forms.Button();
            this.tenKey_6 = new System.Windows.Forms.Button();
            this.tenKey_1 = new System.Windows.Forms.Button();
            this.tenKey_2 = new System.Windows.Forms.Button();
            this.tenKey_3 = new System.Windows.Forms.Button();
            this.tenKey_0 = new System.Windows.Forms.Button();
            this.tenKey_Del = new System.Windows.Forms.Button();
            this.tenKey_7 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.tenKey_8, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_9, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_0, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_Del, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.tenKey_7, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(328, 376);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tenKey_8
            // 
            this.tenKey_8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_8.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_8.Location = new System.Drawing.Point(129, 20);
            this.tenKey_8.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_8.Name = "tenKey_8";
            this.tenKey_8.Size = new System.Drawing.Size(69, 54);
            this.tenKey_8.TabIndex = 1;
            this.tenKey_8.Text = "8";
            this.tenKey_8.UseVisualStyleBackColor = true;
            this.tenKey_8.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_9
            // 
            this.tenKey_9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_9.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_9.Location = new System.Drawing.Point(238, 20);
            this.tenKey_9.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_9.Name = "tenKey_9";
            this.tenKey_9.Size = new System.Drawing.Size(70, 54);
            this.tenKey_9.TabIndex = 2;
            this.tenKey_9.Text = "9";
            this.tenKey_9.UseVisualStyleBackColor = true;
            this.tenKey_9.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_4
            // 
            this.tenKey_4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_4.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_4.Location = new System.Drawing.Point(20, 114);
            this.tenKey_4.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_4.Name = "tenKey_4";
            this.tenKey_4.Size = new System.Drawing.Size(69, 54);
            this.tenKey_4.TabIndex = 3;
            this.tenKey_4.Text = "4";
            this.tenKey_4.UseVisualStyleBackColor = true;
            this.tenKey_4.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_5
            // 
            this.tenKey_5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_5.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_5.Location = new System.Drawing.Point(129, 114);
            this.tenKey_5.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_5.Name = "tenKey_5";
            this.tenKey_5.Size = new System.Drawing.Size(69, 54);
            this.tenKey_5.TabIndex = 4;
            this.tenKey_5.Text = "5";
            this.tenKey_5.UseVisualStyleBackColor = true;
            this.tenKey_5.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_6
            // 
            this.tenKey_6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_6.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_6.Location = new System.Drawing.Point(238, 114);
            this.tenKey_6.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_6.Name = "tenKey_6";
            this.tenKey_6.Size = new System.Drawing.Size(70, 54);
            this.tenKey_6.TabIndex = 5;
            this.tenKey_6.Text = "6";
            this.tenKey_6.UseVisualStyleBackColor = true;
            this.tenKey_6.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_1
            // 
            this.tenKey_1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_1.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_1.Location = new System.Drawing.Point(20, 208);
            this.tenKey_1.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_1.Name = "tenKey_1";
            this.tenKey_1.Size = new System.Drawing.Size(69, 54);
            this.tenKey_1.TabIndex = 6;
            this.tenKey_1.Text = "1";
            this.tenKey_1.UseVisualStyleBackColor = true;
            this.tenKey_1.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_2
            // 
            this.tenKey_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_2.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_2.Location = new System.Drawing.Point(129, 208);
            this.tenKey_2.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_2.Name = "tenKey_2";
            this.tenKey_2.Size = new System.Drawing.Size(69, 54);
            this.tenKey_2.TabIndex = 7;
            this.tenKey_2.Text = "2";
            this.tenKey_2.UseVisualStyleBackColor = true;
            this.tenKey_2.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_3
            // 
            this.tenKey_3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_3.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_3.Location = new System.Drawing.Point(238, 208);
            this.tenKey_3.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_3.Name = "tenKey_3";
            this.tenKey_3.Size = new System.Drawing.Size(70, 54);
            this.tenKey_3.TabIndex = 8;
            this.tenKey_3.Text = "3";
            this.tenKey_3.UseVisualStyleBackColor = true;
            this.tenKey_3.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_0
            // 
            this.tenKey_0.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_0.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_0.Location = new System.Drawing.Point(20, 302);
            this.tenKey_0.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_0.Name = "tenKey_0";
            this.tenKey_0.Size = new System.Drawing.Size(69, 54);
            this.tenKey_0.TabIndex = 9;
            this.tenKey_0.Text = "0";
            this.tenKey_0.UseVisualStyleBackColor = true;
            this.tenKey_0.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_Del
            // 
            this.tenKey_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_Del.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_Del.Location = new System.Drawing.Point(238, 302);
            this.tenKey_Del.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_Del.Name = "tenKey_Del";
            this.tenKey_Del.Size = new System.Drawing.Size(70, 54);
            this.tenKey_Del.TabIndex = 11;
            this.tenKey_Del.Text = "消去";
            this.tenKey_Del.UseVisualStyleBackColor = true;
            this.tenKey_Del.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // tenKey_7
            // 
            this.tenKey_7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tenKey_7.Font = new System.Drawing.Font("MS UI Gothic", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tenKey_7.Location = new System.Drawing.Point(20, 20);
            this.tenKey_7.Margin = new System.Windows.Forms.Padding(20);
            this.tenKey_7.Name = "tenKey_7";
            this.tenKey_7.Size = new System.Drawing.Size(69, 54);
            this.tenKey_7.TabIndex = 0;
            this.tenKey_7.Text = "7";
            this.tenKey_7.UseVisualStyleBackColor = true;
            this.tenKey_7.Click += new System.EventHandler(this.TenKey_Click);
            // 
            // TenKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "TenKey";
            this.Size = new System.Drawing.Size(334, 382);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button tenKey_8;
        private System.Windows.Forms.Button tenKey_9;
        private System.Windows.Forms.Button tenKey_4;
        private System.Windows.Forms.Button tenKey_5;
        private System.Windows.Forms.Button tenKey_6;
        private System.Windows.Forms.Button tenKey_1;
        private System.Windows.Forms.Button tenKey_2;
        private System.Windows.Forms.Button tenKey_3;
        private System.Windows.Forms.Button tenKey_0;
        private System.Windows.Forms.Button tenKey_Del;
        private System.Windows.Forms.Button tenKey_7;
    }
}
