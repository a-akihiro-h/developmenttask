﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DevelopmentTask
{
    public partial class TenKey : UserControl
    {
        private string clickedButtonType;
        private event EventHandler onClickedTenKeyButtonEventHandler;

        public string ClickedButtonType
        {
            get { return this.clickedButtonType;  }
        }

        public event EventHandler OnClickedTenKeyButtonEventHandler
        {
            add
            {
                if (value != null)
                    this.onClickedTenKeyButtonEventHandler += value;
            }

            remove
            {
                if (value != null)
                    this.onClickedTenKeyButtonEventHandler -= value;
            }
        }

        public TenKey()
        {
            InitializeComponent();
        }



        private void TenKey_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            this.clickedButtonType = button.Name.Replace("tenKey_", "");
            this.onClickedTenKeyButtonEventHandler(sender, e);
        }
    }
}
