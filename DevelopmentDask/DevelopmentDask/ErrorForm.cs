﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace DevelopmentTask
{
    public partial class ErrorForm : Form
    {
        private const bool ON = true;//1220
        private const bool OFF = false;//1220

        public bool errReleaseFlag = OFF;
        private string resolveNum;

        public ErrorForm(dynamic error)
        {
            InitializeComponent();

            this.Name = error.ErrNum;
            this.label1.Text = error.Msg;
            resolveNum = error.ResolveNum;
            this.label2.Text =  error.Option;
            this.FormClosing += waitFormExit;
        }

        private void ErrorForm_Load(object sender, EventArgs e)
        {
            this.Icon = SystemIcons.Error;
            Screen s = Screen.FromControl(this);
            this.Size = new Size((int)(s.Bounds.Width*0.4), (int)(s.Bounds.Height*0.4));

            Bitmap canvas = new Bitmap(this.pictureBox1.Width, this.pictureBox1.Height);
            Graphics g = Graphics.FromImage(canvas);

            g.DrawIcon(SystemIcons.Error, this.pictureBox1.Width - SystemIcons.Error.Width, 0);
            this.pictureBox1.Image = canvas;
        }
        private void waitFormExit(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        //button追加0107
        private void errCompleteButton_Click(object sender, EventArgs e)
        {
            if (resolveNum.Equals("notice") || errReleaseFlag)
            {
                this.FormClosing -= waitFormExit;
                this.Close();
                this.Dispose();
                Error.removeFormInstance(this.Name);
            }
            
        }
    }
}
